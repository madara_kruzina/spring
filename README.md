# Spring basics.

We will create application for management corona patiens, doctors and tests using spring.

### App requiriements:

Patient module(Mara un Madara):
* Register patients (name, surname, personalId, email).
* Update patient status and data.
* Request patient for corona tests.
* List all patients.
* Search for patients by name, surname, personalId, status.

Additional:
* Pagination and sorting in list.
* Search patient by name, surname, personal id, email.

Business validation for patient module:
* Do not allow register with duplicate emails.
* Do not allow to register patients from black list (check perosnal_Id).
* Do not allow more than x tests requests  per week. (should be configurable.)

Doctor module(Arturs, Arturs un Edgars):
* Register a doctor. (name, surname, clinic name, doctor_id)
* Assign doctor to patients.
* Send patient for corona tests.
* Accept patient request.
* Auto assigne patient to doctors with less active patiients.

Additional:
* Pagination and sorting in list.
* Search doctor by name, surname, doctor id.

Business validation for doctor module:
* Do not allow to add doctor with same doctor_id
* Do not allow to accept patient requests to corona more than x times per month (configurable).
* Do not allow to assign doctor to patient if doctor has more than x patients (configurable).
* (Optional) Before register a doctor check if its doctor_id exist in doctor registers (possible integration https://www.neslimo.lv/arstniecibas-personas/?search_value={doctor_id} ).

Coronta test module:
* List all tests (with filtering by status).
* Update test results and data.
* Notify patients and doctors about results.

Business validation for coronta test module:
* Do not allow to send for testing if patient test already in progress.
* Do not allow for repeating tests if previous test was made less than x days ago (should be configurable)

Black list module:
* Add clients peronal Id in black list with description.
* Add doctors id in black list with description.
* List all blacklisted persons
* Search blaclisted persons by personal id or doctor id.

Business Validation:
* Do not allow dupicates perosnal id
* Do not allow dupicates doctor id

Web module:

Patient api:
* /api/patients - GET (list all), POST(register)
* /api/patients/{id} - GET (patient status), PUT(Update patient data (email))
* /api/patients/{id}/test - GET (test status), POST(request for test)

Additional task:
* Add pagination and sorting.
* Add search filters.

Doctor api:
* /api/doctors - GET (list all), POST(register)
* /api/doctors/{id} - GET(view data), PUT (update data) 
* /api/doctors/{id}/patients - GET(view all patiens assigned to doctor), POST(assigne new patien)
* /api/doctors/{id}/patients/{patient_id}/ - GET(view patient data), PUT(update patient data)
* /api/doctors/{id}/patients/{patient_id}/test - GET(view patient test requests), POST(send patient to test)
* /api/doctors/{id}/patients/{patient_id}/test/{test_id} - GET(view test status)

Additional task:
* Add pagination and sorting.
* Add search filters.

Corona test api:
* /api/tests GET (list all tests), POST (register new test) - input data (patient_id, doctor_id)
* /api/tests/{id} GET (view test result), PUT (update test result)

Black list api:
* /api/black-list/ - GET (list all), POST(add new entry)
* /api/black-list/{id} - PUT (update entry)
* /api/black-list/{id} - DELETE (delete entry) 

Feedback module:
* /feedback - Page with feedback form.

Security module:
* Register user with roles: ROLE_PATIENT, ROLE_DOCTOR, ROLE_LAB_WORKER, ADMIN
* Add security through filters on endpoints, so that users can only view and edit own data.
* Update registration endpoints to supply password and register users.

Guidelines: 
Structure:
1. Business module
	Each module consist of:
	* Domain objects.
	* Service query service or business logic service.
	* Businiess validators.
	* Repository.

2. Web module:
	Contains all endpoints and have following sturcture.
	* Request dto.
	* Response dto.
	* Rest controllers.
	* Some static pages.

3. Security module:
	Contains needed classes to implement security
	* User entity and repositories
	* Security configuration

Web Controller responsibility:
1. Validate request input parameters(if needed)
2. Map input to business objects needed for services (if needed)
3. Call necessary services
4. Map result from services to response (if needed)
5. Return response or throw exception

Service responsibility:
1. Perform mapping to other objects (if needed)
2. Perform business validation of input (if needed)
3. Fetch necessary busniess data from repositories (if needed)
4. Perform business logic or call other services to perform this logic.
5. Map result(if needed) to output objects
6. return result or throw exception

Repository responsibility:
1. Validate inputs (if needed)
2. Perform neccessary db operations (read, create, update, delete)
3. perform mapping (if needed)
4. return result or throw exception