package sda.java10.covid.web.patients;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import sda.java10.covid.patients.PatientEntity;
import sda.java10.covid.patients.PatientRepository;
import sda.java10.covid.patients.PatientService;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
public class PatientController {
    PatientRepository patientRepository;
    PatientService patientService;

    public PatientController(PatientRepository patientRepository, PatientService patientService) {
        this.patientRepository = patientRepository;
        this.patientService = patientService;
    }

    @GetMapping("/api/patients")
    public List<PatientDto> getAllPatients() {
        List<PatientEntity> allPatients = patientRepository.findAll();
        return allPatients.stream().map(new PatientDtoMapper()).collect(Collectors.toList());
    }

    @PostMapping("/api/patients")
    public PatientDto registerPatient(@RequestBody PatientRegistrationDto patient ) {
        PatientEntity patientEntity = patientService.registerPatient(patient.getFirstName(),
                patient.getLastName(),
                patient.getPersonalId(),
                patient.getEmail());

        return new PatientDtoMapper().apply(patientEntity);

    }

    public static class PatientDtoMapper implements Function<PatientEntity, PatientDto> {
        @Override
        public PatientDto apply(PatientEntity patientEntity) {
            PatientDto patientDto = new PatientDto();
            patientDto.setFirstName(patientEntity.getFirstName());
            patientDto.setLastName(patientEntity.getLastName());
            patientDto.setPatientStatus(patientEntity.getStatus());
            patientDto.setId(patientEntity.getId());
            return patientDto;
        }
    }

}
