package sda.java10.covid.patients;

public enum PatientStatus {
    REGISTERED,
    SENT_TO_TEST,
    TEST_NEGATIVE,
    TEST_POSITIVE
}
