package sda.java10.covid.patients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sda.java10.covid.patients.validator.PatientValidator;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);

    private final PatientRepository patientRepository;
    private List<PatientValidator> allValidators;

    public PatientService(PatientRepository patientRepository, List<PatientValidator> allValidators) {
        this.patientRepository = patientRepository;
        this.allValidators = allValidators;
    }

    @Transactional
    public PatientEntity registerPatient(String firstName, String lastName, String personalId, String email) {
        LOGGER.info("Registering patient: {} {} {} {}.", firstName, lastName, personalId);
        PatientEntity patient = new PatientEntity(firstName, lastName, personalId, email);
        patient.setStatus(PatientStatus.REGISTERED);

        allValidators.forEach(patientValidator -> {
            if (!patientValidator.isValid(patient)) {
                throw new IllegalArgumentException("Bad PatientEntity");
            }
        });

        // Moved after validator because @Transactional rollback
        // is not working for now.
        patientRepository.save(patient);

        return patient;
    }

    @Transactional
    public void changePatientStatus(PatientEntity patient, PatientStatus status) {
        LOGGER.info("Updating status of {} {} to {}.", patient.getFirstName(), patient.getLastName(), status);
        patient.setStatus(status);
        patientRepository.save(patient);
    }

    @Transactional
    public void sendPatientToTest(PatientEntity patient) {
        LOGGER.info("Sending {} {} to test", patient.getFirstName(), patient.getLastName());
        patient.setStatus(PatientStatus.SENT_TO_TEST);
        patientRepository.save(patient);
    }

    @Transactional
    public void performTestOnYourOwn(PatientEntity patient) {
        LOGGER.info("{} {} testing for Covid-19", patient.getFirstName(), patient.getLastName());
        Random random = new Random();
        List<PatientStatus> patientStatuses = new ArrayList<>();
        patientStatuses.add(PatientStatus.TEST_POSITIVE);
        patientStatuses.add(PatientStatus.TEST_NEGATIVE);
        PatientStatus testResult = patientStatuses.get(random.nextInt(patientStatuses.size()));
        if (testResult.equals(PatientStatus.TEST_POSITIVE)) {

            patient.setStatus(PatientStatus.TEST_POSITIVE);
        }
        if (testResult.equals(PatientStatus.TEST_NEGATIVE)) {
            patient.setStatus(PatientStatus.TEST_NEGATIVE);

        }

    }

    public List<PatientEntity> findPatientByName(String name) {
        return patientRepository.findByFirstName(name);
    }

    public List<PatientEntity> findPatientByLastName(String lastName){
        return patientRepository.findByLastName(lastName);
    }

    public PatientEntity findOnePatientByLastName(String lastName){
        return patientRepository.findOneByLastName(lastName).get();
    }

    public List<PatientEntity> findPatientByPersonalId(String personalId) {
        return patientRepository.findAll().stream()
                .filter(patient -> patient.getPersonalId().equals(personalId))
                .collect(Collectors.toList());
    }

    public List<PatientEntity> findPatientByStatus(String status) {
        return patientRepository.findAll().stream()
                .filter(patient -> patient.getStatus().equals(status))
                .collect(Collectors.toList());

    }

    public List<PatientEntity> listAllRegisteredPatients() {
        List<PatientEntity> allPatients = patientRepository.findAll();
        return allPatients.stream()
                .filter(patient -> patient.getStatus() == PatientStatus.REGISTERED)
                .collect(Collectors.toList());

//        List<PatientEntity> registeredPatients = new ArrayList<>();
//        for (PatientEntity patient : allPatients) {
//            if (patient.getStatus() == PatientStatus.REGISTERED){
//                registeredPatients.add(patient);
//            }
//        }
//        return registeredPatients;
    }

}
