package sda.java10.covid.patients;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<PatientEntity, Long> {


    //@Query(value = "select * from patients where first_name = :firstName", nativeQuery = true)
    //@Query(value = "from PatientEntity p where p.firstName = :firstName")
    List<PatientEntity> findByFirstName(String firstName);

    List<PatientEntity> findByLastName(String lastName);

    Optional<PatientEntity> findOneByLastName(String lastName);
}
