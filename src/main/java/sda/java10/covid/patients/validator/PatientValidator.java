package sda.java10.covid.patients.validator;

import sda.java10.covid.patients.PatientEntity;

public interface PatientValidator {
    boolean isValid(PatientEntity patient);

}
