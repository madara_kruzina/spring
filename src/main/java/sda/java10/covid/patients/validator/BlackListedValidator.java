package sda.java10.covid.patients.validator;

import org.springframework.stereotype.Component;
import sda.java10.covid.blacklist.BlackListService;
import sda.java10.covid.patients.PatientEntity;

import java.util.List;

@Component
public class BlackListedValidator implements PatientValidator {
    private final BlackListService blackListService;

    public BlackListedValidator(BlackListService blackListService) {
        this.blackListService = blackListService;
    }

    @Override
    public boolean isValid(PatientEntity patient) {
        List<String> blackListedSSNs = blackListService.getPersonalIdList();
        return !blackListedSSNs.contains(patient.getPersonalId());
    }

}
