package sda.java10.covid.patients.validator;

import org.springframework.stereotype.Component;
import sda.java10.covid.patients.PatientEntity;

@Component
public class GobzemsValidators implements PatientValidator {

    @Override
    public boolean isValid(PatientEntity patient) {
        return !patient.getLastName().equals("Gobzems");
    }
}
