package sda.java10.covid.blacklist;

import java.util.List;

public interface BlackListRepository {

     void saveBlackList(BlackListEntity blackList);
     List<BlackListEntity> getAll();
}
