package sda.java10.covid.blacklist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Component
public class JpaBlackListRepository implements BlackListRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveBlackList(BlackListEntity blackList) {
        entityManager.persist(blackList);
    }

    @Override
    public List<BlackListEntity> getAll() {
        return entityManager.createQuery("from BlackListEntity", BlackListEntity.class).getResultList();
    }

}
