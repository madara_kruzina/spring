package sda.java10.covid.blacklist;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BlackListService {
    private final BlackListRepository blackListRepository;

    public BlackListService(BlackListRepository blackListRepository) {
        this.blackListRepository = blackListRepository;
    }

    public List<String> getPersonalIdList() {

        return blackListRepository.getAll().stream().map(BlackListEntity::getPersonalId).collect(Collectors.toList());
    }

    @Transactional
    public void addPersonalId(String personalId) {
        BlackListEntity blackListEntity = new BlackListEntity();
        blackListEntity.setPersonalId(personalId);
        blackListRepository.saveBlackList(blackListEntity);
    }


}
