package sda.java10.covid.patients;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import sda.java10.covid.AbstractSpringTest;
import sda.java10.covid.blacklist.BlackListService;

import java.util.List;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class PatientServiceTest extends AbstractSpringTest {

    @Autowired
    private PatientService patientService;

    // Only way to inject beans in a test is with
    // @Autowired.
    @Autowired
    private BlackListService blackListService;

    @Test
    public void shouldRegisterPatient() {
        // When:
        patientService.registerPatient("Tom", "Bombadil", "456454-456445", "tom@gmail.com");

        // Then:
        List<PatientEntity> patients = patientService.listAllRegisteredPatients();

        // Has anything.
        assertTrue(patients.size() > 0);

        PatientEntity tom = patients.get(0);
        assertEquals(tom.getFirstName(), "Tom");
        assertEquals(tom.getLastName(), "Bombadil");
        assertEquals(tom.getPersonalId(), "456454-456445");
        assertEquals(tom.getEmail(), "tom@gmail.com");
        assertEquals(tom.getStatus(), PatientStatus.REGISTERED);
    }

    @Test
    public void shouldNotRegisterPatientWithLastNameGobzems() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            patientService.registerPatient("John", "Gobzems", "123456-1234567", "john@gmail.com");
        });
        assertEquals(exception.getMessage(), "Bad PatientEntity");
    }

    @Test
    public void shouldNotRegisterBlackListedPatient() {
        String badID = "42069";
        blackListService.addPersonalId(badID);

        IllegalArgumentException exception =  assertThrows(IllegalArgumentException.class, () -> {
            patientService.registerPatient("Tom", "Bad", "1111-2222", badID);
        });

        assertEquals(exception.getMessage(), "Bad PatientEntity");
    }

    @Test
    public void shouldChangePatientsStatus(){

        PatientEntity testPatient = patientService.registerPatient("Maria","Blau", "9999-1234", "maria@gmail.com");
        assertEquals(testPatient.getStatus(), PatientStatus.REGISTERED);
        patientService.changePatientStatus(testPatient,PatientStatus.TEST_POSITIVE);
        assertEquals(testPatient.getStatus(), PatientStatus.TEST_POSITIVE);
    }

    @Test
    public void shouldChangePatientsStatus2() {

        PatientEntity testPatient = patientService.registerPatient("Zita", "Kanna", "3333-4444", "zita@gmail.com");
        assertEquals(testPatient.getStatus(), PatientStatus.REGISTERED);
        patientService.changePatientStatus(testPatient, PatientStatus.TEST_NEGATIVE);
        assertEquals(testPatient.getStatus(), PatientStatus.TEST_NEGATIVE);

    }

    @Test
    public void sentToTest() {

        PatientEntity testPatient = patientService.registerPatient("Laura", "Liepa", "5555-1234", "laura@gmail.com");
        assertEquals(testPatient.getStatus(), PatientStatus.REGISTERED);
        patientService.changePatientStatus(testPatient, PatientStatus.SENT_TO_TEST);
        assertEquals(testPatient.getStatus(), PatientStatus.SENT_TO_TEST);

    }

    @Test
    public void findByNameShouldFindAllSameNamePatients(){
        patientService.registerPatient("Anna","Kanna", "134000", "anna@gmail.com");
        patientService.registerPatient("Maija","Kanna", "134000", "anna@gamail.com");
        patientService.registerPatient("Anna","Kanna", "134000", "anna@maail.com");

        List<PatientEntity> allTestNamePatients = patientService.findPatientByName("Anna");
        assertTrue(allTestNamePatients.size()==2);
    }

    @Test
    public void findByLastNameShouldFindAllSameNamePatients(){
        patientService.registerPatient("Anna","Kanna", "134000", "anna@gmail.com");
        patientService.registerPatient("Maija","Kanna", "134000", "anna@gamail.com");
        patientService.registerPatient("Anna","Kanna", "134000", "anna@maail.com");

        List<PatientEntity> allTestNamePatients = patientService.findPatientByLastName("Kanna");
        assertTrue(allTestNamePatients.size()==3);
    }

    @Test
    public void shouldFindByLastName(){
        patientService.registerPatient("Anna","Kanna2", "134000", "anna@gmail.com");

        PatientEntity patient = patientService.findOnePatientByLastName("Kanna");
        assertEquals(patient.getLastName(), "Kanna2");
    }


    @Test
    public void shouldSendPatientToTest(){

        PatientEntity testPatient = patientService.registerPatient("Anna","Kanna", "134000", "mail");
        assertEquals(testPatient.getStatus(), PatientStatus.REGISTERED);
        patientService.sendPatientToTest(testPatient);
        assertEquals(testPatient.getStatus(), PatientStatus.SENT_TO_TEST);
    }
}